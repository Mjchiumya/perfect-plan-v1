<?php

/**
 * AuthModel short summary.
 *
 * AuthModel description.
 *
 * @version 1.0
 * @author acer
 */
class AuthModel extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->database();
	}

    //register user
    public function register_user($user){
        $this->db->insert('user', $user);
    }
    //login user
    public function login_user($email,$pass){
        $this->db->select('u_email,u_name,u_id');
        $this->db->from('user');
        $this->db->where('u_email',$email);
        $this->db->where('u_password',$pass);
        if($query=$this->db->get())
        {
            return $query->row_array();
        }
        else{
            return false;
        }
    }
    //check user email if unique
    public function email_check($email){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('u_email',$email);
        $query=$this->db->get();
        if($query->num_rows()>0){
            return false;
        }else{
            return true;
        }
    }
    //deactivate account
    public function account_deactivate($uid){
        $this->db->set('u_password','deactivated');
        $this->db->where('u_id',$uid);
        $this->db->update('user');
        return true;
    }


}
