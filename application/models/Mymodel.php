<?php

class Mymodel extends CI_Model{

	  function __construct(){
        parent::__construct();
        $this->load->database();
	}

public function get_business_services($bid) {
   $this->db->select('*');
     $this->db->where('b_id',$bid);
      $query = $this->db->get('service');
        return $query->result();
        }

 function getRows($params = array()){
        $this->db->select('*');
        $this->db->from('service');
        if(array_key_exists("s_id",$params)){
            $this->db->where('s_id',$params['id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $this->db->count_all_results();
            }else{
                $query = $this->db->get();
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        //return fetched data
        return $result;
   }

public function get_all() {
   $this->db->select('*');
     $this->db->limit(12);
      $query = $this->db->get('service');
        return $query->result();
    }

public function get_services($eid) {
   $this->db->select('*');
     $this->db->where('eid',$eid);
      $query = $this->db->get('eventprofile');
        return $query->result();
        }

public function get_three() {
        $this->db->select('*');
        $this->db->limit(6);
        $query = $this->db->get('service');
        return $query->result();
    }
public function get_categories() {
        $this->db->select('s_category');
         $this->db->distinct('s_category');
		  $this->db->limit(10);
        $query = $this->db->get('service');
        return $query->result();
    }
public function get_demography() {
        $this->db->select('s_demography');
         $this->db->distinct('s_demography');
		    $this->db->limit(10);
          $query = $this->db->get('service');
        return $query->result();
    }
public function get_business() {
        $this->db->select('b_name,b_id');
         $this->db->distinct('b_name');
		   $this->db->limit(10);
        $query = $this->db->get('business');
        return $query->result();
    }
public function get_businessId($bid) {
        $this->db->select('*');
         $this->db->where('b_id',$bid);
        $query = $this->db->get('business');
        return $query->result();
    }
public function get_serviceId($sid) {
        $this->db->select('*');
         $this->db->where('s_id',$sid);
        $query = $this->db->get('service');
        return $query->result_array();
    }
public function get_service(){
       $this->db->select('s_name,s_id');
         $this->db->distinct('s_name');
		  $this->db->limit(10);
        $query = $this->db->get('service');
        return $query->result();
}
public function all_services(){
    	 $query = $this->db->get('service');
       $service_array = $query->result_array();
       return $service_array;
    }

public function all_events(){
    	 $query = $this->db->get('event');
       $event_array = $query->result_array();
       return $event_array;
    }

public function load_db_cart($eid){
    $this->db->select('*');
     $this->db->from('eventprofile');
      $this->db->where('eid',$eid);
       $this->db->join('service','eventprofile.sid = service.s_id');
       $query = $this->db->get();
       if ($query != null) {
      return $query->result_array();
    }
    return false;
}

public function serviceOrder($eid){
    $this->db->select("*");
	 $this->db->from('eventprofile');
	  $this->db->where('eid',$eid);
	   $query = $this->db->get();
	     if($query->num_rows() > 0){
	        $this->db->set('e_status','created');
	         $this->db->where('e_id',$eid);
	          $this->db->update('event');
			  return true;
	   }else if($query->num_rows() <= 0){
	              return false;
	                    }

	 return false;
}

public function orderCancel($eid){
	$this->db->set('e_status','cancelled');
	$this->db->where('e_id',$eid);
	$this->db->update('event');
}
public function cartdbmode($sid,$qty,$total,$currentEventId){

     //verify event exist before adding service
      $this->db->select('*');
       $this->db->from('event');
        $this->db->where('e_id',$currentEventId);
         $query = $this->db->get();

     if(!$query){
          return false;
          }else{
		   //get num row
		     $this->db->select('*');
              $this->db->from('eventprofile');
                $qry = $this->db->get();
				 $row = $qry->last_row();

				  $data = array(
		            'sid' =>intval($sid),
                    'qty'=>$qty,
                    'total'=>$total,
                    'eid'=> $currentEventId
                     );
                 $this->db->insert('eventprofile', $data);
        }
   }
}
