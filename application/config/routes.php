<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//pages
$route['default_controller'] = 'pagesController';
$route['home'] = 'pagesController/index';
$route['coming'] = 'pagesController/comingsoon';
$route['login']='pagesController/login_view';
$route['register']='pagesController/register_view';
$route['user']='pagesController/user_profile';
//auth
$route['register_user']='AuthController/register_user';
$route['login_user']='AuthController/login_user';
$route['logout']='AuthController/user_logout';
$route['deactivate/(:num)']='AuthController/user_deactivate/$1';
//business
$route['service/(:num)']='MyController/serviceview/$1';
$route['business/(:num)']='MyController/businessview/$1';
$route['cancel-order/(:num)']='MyController/cancelOrder/$1';
$route['order/(:num)'] = 'MyController/orderServices/$1';
//event
$route['create']='eventController/create_view';
$route['eventprofile']='eventController/event_input';
$route['editevent/(:num)']='eventController/event_editor/$1';
$route['deleteevent/(:num)']='eventController/del_event/$1';
$route['event/(:num)']='eventController/eventview/$1';
$route['live-event/(:num)']='eventController/liveEventView/$1';
//cart
$route['addservice/(:num)']='cartController/create_cart/$1';
$route['cartadd']='cartController/addToCart';
$route['cartitemdel']='cartController/removeItem';
$route['cartload']='cartController/loadCart';
$route['clear']='cartController/clearCart';
$route['form']='MyController/event_form';
$route['formevtedit']='MyController/evtedit_formval';
$route['eventreg/(:num)']='cartController/cartdb/$1';
//security/404
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
