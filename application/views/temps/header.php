<!DOCTYPE html>
  <html lang="en">
    <head>      
       <title>Perfect Plan</title>
       <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- Compiled and minified CSS -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
      <link rel="stylesheet" href="<?php echo base_url();?>/css/main.css">
      <link href="https://fonts.googleapis.com/css?family=Ubuntu+Condensed" rel="stylesheet">
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">          
    </head>

<body>    	
 <header>
  <div class="right plogo">
    <a href="<?php echo base_url();?>"><img class="right" width="120" height="80" src="<?php echo base_url();?>img/sys/logo.png"></a>
  </div>

   <ul id="slide-out" class="sidenav">
        <br>
    <li id="li"><a href="<?php echo base_url();?>"><i class="material-icons">store</i>Home</a></li>    
    <li id="li"><a href="<?php echo base_url('aboutus');?>"><i class="material-icons">info</i>About us</a></li>
    <li><a href="<?php echo base_url('coming');?>"><i class="material-icons">receipt</i>Blog</a></li>
    <li><a href="<?php echo base_url('coming');?>"><i class="material-icons">help</i>Help</a></li>
    <li><a href="<?php echo base_url('eventprofile');?>"><i class="material-icons">event</i>Create Event</a></li>
    <li><a href="<?php echo base_url('coming');?>"><i class="material-icons">mail</i>Contact Us</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Accounts</a></li>
     <li><a href="<?php echo base_url('user');?>"><i class="material-icons">person</i>My Account</a></li>
   
    <li><center><a class="btnc grey" href="<?php echo base_url('login');?>">login </a>
  <a class="btnc grey" href="<?php echo base_url('register');?>">join</a>
  </center></li> 
  </ul>
  <br>

  <h6 class="menu"> <a href="#" data-target="slide-out" class="sidenav-trigger pulse"><i class="material-icons white-text">menu</i></a></h6>  
   </header>
   <br> 
     
