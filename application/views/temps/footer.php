
      <br>
    <footer class="footer-bs">
        <div class="row">
        	<div class="col m3 hide-on-small-only footer-brand animated fadeInLeft">
            	<h4>Perfect Plan</h4>
                <p>Suspendisse hendrerit tellus laoreet luctus pharetra. Aliquam porttitor vitae orci nec ultricies. Curabitur vehicula, libero eget faucibus faucibus, purus erat eleifend enim, porta pellentesque ex mi ut sem.</p>                
                <em class="white-text"> 20<?php echo date('y');?> perfect plan©</em>
            </div>
        	<div class="col m4 footer-nav animated fadeInUp">
            	<h4>Menu</h4>
            	<div class="col m6">
                    <ul class="pages">
                        <li><a href="#">About</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Contact us</a></li>                      
                    </ul>
                </div>

            	<div class="col m6">
                    <ul class="list">
                        <li><a href="#">Business</a></li>                       
                        <li><a href="#">Terms & Condition</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>

        	<div class="col m2 footer-social animated fadeInDown">
            	<h4>Follow Us</h4>
            	<ul>
                	<li><a href="#">Facebook</a></li>
                	<li><a href="#">Twitter</a></li>
                	<li><a href="#">Instagram</a></li>
                	<li><a href="#">RSS</a></li>
                </ul>
            </div>
            
        	<div class="col m3 footer-ns animated fadeInRight">
            	<h4>Newsletter</h4>
                <p>Subscribe for our news letter</p>
               
                    <div class="input-group">
                      <input type="text" class="form-control" placeholder="Email...">
                      <span class="input-group-btn">
                        <button class="btn btn-default red" type="button"><span class="glyphicon glyphicon-envelope">subscribe</span></button>
                      </span>
                    </div><!-- /input-group -->
               
            </div>
        </div>
        
    </footer>
     
      <!-- Compiled and minified JavaScript -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/jquery/jqueryc.js"></script>  

    </body>
  </html>