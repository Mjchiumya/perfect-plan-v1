<div class="base col s12">
  <div class="row">
    <div class="col s12">
      <ul class="tabs">
        <li class="tab col s4">
          <a href="#test1">Account</a>
        </li>
        <li class="tab col s4">
          <a class="active" href="#test2">Events</a>
        </li>        
        <li class="center-align col s4">
          <a href="<?php echo base_url('logout');?>" class="btn red">Logout</a>
        </li>
      </ul>
    </div>
   </div> 

  <div id="test1" class="col s12"> 
   <div class="wrup grey">
    <div class="row white">     
      <div class="col s12">
	    <div class="panel ">
		 <ul class="list">
		  <li>
		  <div class="valign-wrapper">
               <i class="material-icons circle large grey-text">person</i>
                 <div class="title">
				   <h4 class="prim">Welcome
                   <span><?php echo $this->session->userdata('user_name');?></span><br/>				   
                   </h4>
                </div>
                 <i class="material-icons ml-auto">visibility</i>
             </div>
			 </li>
		 </ul> 
       </div>   
	   
	   <div class="card-panel row">	   
	    <div class="col"><a href="#" class="prim"><i class="material-icons left grey-text">email</i>Email</a></div>
		<div class="col"><a href="#" class="prim"><i class="material-icons left grey-text">phone</i>0976 3221 11</a></div>
		<div class="col"><a href="#" class="prim"><i class="material-icons left grey-text">people</i>12 May 2019</a></div>
		<a href="<?php echo base_url('deactivate').'/'.$this->session->userdata('user_id');?>" class="right-align btn grey white-text">deactivate account</a>
    </div>
      
</div>
</div>
</div>
</div>
<div id="test2" class="col s12">
<div class="container">
  <div class="row">     
        
       <div class="evtcard">
	   <h3 class="subheader">alerts</h3>
	     <?php     
           $msg = $this->session->flashdata('order_msg');          
         if($msg){
           ?>
          <div class="green-text">
           <?php echo $msg; ?>
              </div>
                <?php
                  }?>
         <h3 class="subheader">Events</h3>
          <ul class="list">
           <?php foreach($evt as $ev){
              
            echo '
            <li class="waves-effect">
			   <a class="dropdown-button" 
           href="'.base_url().'live-event/'.$ev->e_id.'"> 
              <div class="valign-wrapper">
               <i class="material-icons left circle white-text">folder</i>
                 <div class="title">                  
                   '.$ev->e_name.'<br>
                 <span>'.$ev->e_date.'</span>
                </div>
                 <i class="material-icons ml-auto">info</i>
             </div>
			 </a>
         </li>';
       }?>            
    </ul>
    <hr class="list-divider">
    <h3 class="subheader">Draft</h3>
    <ul class="list">
    <?php foreach($drafts as $draft){
   
echo '<li class="waves-effect">
        <a class="dropdown-button" 
           href="'.base_url().'event/'.$draft->e_id.'">        
              <div class="valign-wrapper">
               <i class="material-icons left circle white-text">
                   insert_drive_file
                   </i>
                 <div class="title">
                 '.$draft->e_name.'<br>
                  <span>'.$draft->e_date.'</span>
              </div>          
            <i class="material-icons ml-auto">info</i>            
        </div>      
      </a>     
      </li>';

    }?>
   </ul>
   <h3 class="subheader">Cancelled</h3>
          <ul class="list">
           <?php foreach($cancel as $exed){
              
            echo '
            <li class="waves-effect">
			   <a class="dropdown-button" 
           href="#"> 
              <div class="valign-wrapper">
               <i class="material-icons left circle white-text">folder</i>
                 <div class="title">                  
                   '.$exed->e_name.'<br>
                 <span>'.$exed->e_date.'</span>
                </div>
                 <i class="material-icons ml-auto">info</i>
             </div>
			 </a>
         </li>';
       }?>            
    </ul>
    <hr class="list-divider">
  </div>   
    </div>      
      
  </div>
</div>
</div>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
<script type="text/javascript"  src="<?php echo base_url();?>/jquery/jqueryc.js"></script>     
</body>
</html>
