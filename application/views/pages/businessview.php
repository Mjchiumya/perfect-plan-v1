<div class="base">

  <div class="container hyt">
     <?php foreach($business as $bus):
	          echo '<img alt="perfect-plan-malawi" class="responsive-img" style="height:40rem;" width="100%" src="'.base_url().'img/business/'.$bus->b_img.'">';?>
    
  </div>
  <div class="section"></div>
  <div class="container business bgr white">
   <a class=" indigo lighten-5 btnc modal-trigger right  grey-text text-darken-4" href="#modal1">see our services</a>
   <?php 
	echo '
	    <h3 class="header">'.$bus->b_name.'<br><span class="grey-text text-darken-1 small-text">'.$bus->b_location.'</span></h3>
		<div class="section"></div>
		<p class="left-align grey-text text-darken-4">'.$bus->b_bio.'</p>		

		<div class="btnc indigo lighten-5">
		<div class="col"><a href="#" class="grey-text text-darken-3"><i class="material-icons left grey-text text-darken-3">email</i>'.$bus->b_email.'</a></div>
		<div class="col"><a href="#" class="grey-text text-darken-3"><i class="material-icons left grey-text text-darken-3">perm_phone_msg</i>'.$bus->b_phone.'</a></div>
		<div class="col"><a href="#" class="grey-text text-darken-3"><i class="material-icons left grey-text text-darken-3">public</i>'.$bus->b_social.'</a></div>
		</div> ';
		
		
		endforeach;?>
		
  </div>
</div>

 <!-- Modal Trigger -->
 

  <!-- Modal Structure -->
  <div id="modal1" class="modal bottom-sheet">
    <div class="modal-content">      
	  <div class="flex-container col s12">
		<div class="row"> <?php foreach($services as $service){
	     echo ' <div class="col s6 m3 flip-box">
		<div class="flip-box-inner">
		<div class="flip-box-front"> 
		<img class="responsive-img" width="100%" src="'.base_url().'img/services/'.$service->image.'">
		</div>
		<div class="flip-box-back"> <h3>'.$service->s_name.'</h3>
		<div class="section hide-on-small-only"></div> 
		<a href="'.base_url().'service/'.$service->s_id.'" class="btnc success">view</a> 
		</div>
		</div> 
		</div>';}?>
<!-- end card-tree -->
</div >
</div >


  </div>

    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">close</a>
    </div>
  </div>
