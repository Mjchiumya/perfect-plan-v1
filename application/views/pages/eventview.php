<div class="section"></div>
<div class="container">
<div class="base">

<div class="col s12 bg-image">
<div class="section"></div>
<div class="section"></div>
<div class="bg-text">
<?php foreach($event as $ev):?>  
  <h2><?php echo $ev->e_location;?></h2>
  <h1 style="font-size:50px"><?php echo $ev->e_name;?></h1>
  
  <p>Draft Event organised by <span class="white-text"><?php echo $ev->e_organiser;?></span></p>
</div>
</div>

	  <div class="col s12">
      <ul class="collapsible" data-collapsible="accordion">  
       <li>
         <div class="collapsible-header">
          <i class="material-icons">info</i>Details       
         </div>
            <div class="collapsible-body">       
             <ul class="collection with-header">                     
              
               <li class="collection-header ">
               <h4><?php echo $ev->e_name;?></h4>
              </li>
              <li class="collection-item">
          <div>
          <?php echo $ev->e_location;?>
            <a href="#!" class="grey-text">
              <i class="material-icons left">location_on</i>
            </a>
          </div>
        </li>
        <li class="collection-item">
          <div>
            <?php echo $ev->e_date;?>
            <a href="#!" class="grey-text">
              <i class="material-icons left">today</i>             
          </a>
        </div>
      </li>
        <li class="collection-item">
          <div>
           <?php echo $ev->e_time;?>
            <a href="#!" class="grey-text">
              <i class="material-icons left">query_builder</i>
            </a>
          </div>
        </li>
        <li class="collection-item">
          <div>
            <?php echo $ev->e_charge;?>
            <a href="#!" class="grey-text">
              <i class="material-icons left">monetization_on</i>
            </a>
          </div>
        </li>
        <li class="collection-item">
          <div>
            <?php echo $ev->e_type;?>
            <a href="#!" class="grey-text">
              <i class="material-icons left">group</i>
            </a>
          </div>
        </li>
      
      </ul>
         <?php endforeach;?>
      </div>     
    </li>   
    <li>
      <div class="collapsible-header">
        <i class="material-icons">chrome_reader_mode</i>
      services</div>
      <div class="collapsible-body servix">
      
  <div class="table-responsive">
   
   <table class="table table-bordered">
    <tr>
     <th width="25%">Name</th>
     <th width="25%">Quantity</th>
     <th width="25%">Price</th>
     <th width="25%">Total</th>     
    </tr>

  <?php foreach($products as $items){
   echo '
   <tr> 
    <td>'.$items["s_name"].'</td>
    <td>'.$items["qty"].'</td>
    <td>'.$items["s_price"].'</td>
    <td>'.$items["total"].'</td>    
   </tr>';
   }?>
   </table>
   </div>
   </div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">whatshot</i>help</div>
      <div class="collapsible-body">
	  <span>
	  Add services to your event by clicking 'service' below.<br>
	  press 'edit' below to edit event details and 'del' to permanatly delete event.<br>
	  once you send the order your event will be live and order dispatched.<br>
	  All terms and condition apply
	  
	  </span></div>
    </li>
  <li>
<nav>
    <div class="nav-wrapper grey">
      <div class="col s12">
        <?php foreach($event as $ev):
        echo '         
        <a href="'.base_url().'editevent/'.$ev->e_id.'" class="breadcrumb">Edit</a>
        <a href="'.base_url().'deleteevent/'.$ev->e_id.'" class="breadcrumb">Del</a>
        <a href="'.base_url().'addservice/'.$ev->e_id.'" class="breadcrumb">Service</a>
        ';?>

  <div class="right">
    <a href="<?php echo base_url().'order/'.$ev->e_id;?>" class="btnc">send order</a>
  </div>
  <?php endforeach;?>
      </div>
    </div>
 </nav>
 </li>     
  </ul>
  <a href="<?php echo base_url();?>user" class="btn red">Back<i class="material-icons right">replay</i></a>
  
     </div>  
   </div> 

</div>
</div>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
<script type="text/javascript"  src="<?php echo base_url();?>/jquery/jqueryc.js"></script>     
</body>
</html>
