<!DOCTYPE html>
  <html lang="en">
    <head>      
       <title>Perfect Plan</title>
       <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- Compiled and minified CSS -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
      <link rel="stylesheet" href="<?php echo base_url();?>/css/main.css">
      <link href="https://fonts.googleapis.com/css?family=Ubuntu+Condensed" rel="stylesheet">
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">          
    </head>

<body>
<header>
  <div class="right plogo">
    <a href="<?php echo base_url();?>"><img class="right" width="120" height="80" src="<?php echo base_url();?>img/sys/logo.png"></a>
  </div>

   <ul id="slide-out" class="sidenav">
        <br>
    <li id="li"><a href="<?php echo base_url();?>"><i class="material-icons">store</i>Home</a></li>    
    <li id="li"><a href="<?php echo base_url('aboutus');?>"><i class="material-icons">info</i>About us</a></li>
    <li><a href="<?php echo base_url('coming');?>"><i class="material-icons">receipt</i>Blog</a></li>
    <li><a href="<?php echo base_url('coming');?>"><i class="material-icons">help</i>Help</a></li>
    <li><a href="<?php echo base_url('eventprofile');?>"><i class="material-icons">event</i>Create Event</a></li>
    <li><a href="<?php echo base_url('coming');?>"><i class="material-icons">mail</i>Contact Us</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Accounts</a></li>
     <li><a href="<?php echo base_url('user');?>"><i class="material-icons">person</i>My Account</a></li>
   
    <li><center><a class="btnc grey" href="<?php echo base_url('login');?>">login </a>
  <a class="btnc grey" href="<?php echo base_url('register');?>">join</a>
  </center></li> 
  </ul>
  <br>

  <h6 class="menu"> <a href="#" data-target="slide-out" class="sidenav-trigger pulse"><i class="material-icons white-text">menu</i></a></h6>  
   </header>
   <br> 
     

<div class="section"></div>
<div class="section"></div>
<div class="section"></div> 

  <div class="right stick">
    <a class="orange lighten-2 waves-effect waves-light btn modal-trigger" href="#modal1">view</a>
  <a class="orange lighten-2 waves-effect waves-light btn modal-trigger" href="#modal2">filter</a>
  </div> 
  <div class="section"></div>
<div class="wrapper">
  <!-- Modal Structure -->
  <div id="modal1" class="modal modal-fixed-footer">
    <div class="modal-content">
      <div id="cart_details" class="white">
        <h3 align="center">Event is Empty</h3>
       </div>
    </div>
    <div class="modal-footer">      
      <a href=" <?php echo base_url()."eventreg/".$eid['eid'];?>" class="modal-close waves-effect waves-green btn-flat">Save</a>
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">close</a>
    </div>
  </div>

   <!-- Modal filter -->
  <div id="modal2" class="modal modal-fixed-footer">
    <div class="modal-content">
      <div id="cart_details" class="white">
        <div class="input-field col s12">
    <form action="#">
    
  </form>
        
  </div>
   </div>
    </div>
    <div class="modal-footer">      
      <a href=" <?php echo base_url()."eventreg/".$eid['eid'];?>" class="modal-close waves-effect waves-green btn-flat">Save</a>
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">close</a>
    </div>
  </div>
   
 <section class="row">
 
    <?php
      foreach($services as $service) {
       echo '
      
<div class="card small col s6 m4 " >
 <div class="card-image waves-effect waves-block waves-light" >
  <img class="activator" src="'.base_url().'img/services/'.$service->image.'" width="100%">
    <span class="card-title grey truncate hide-on-small-only">'.ucfirst($service->s_category).'</span>
</div >
<div class="card-content" >
   <span class="card-title activator grey-text text-darken-4" > '.$service->s_name.'<i class="material-icons right" > more_vert</i > </span >
</div>
<div class="card-reveal" >
<span class="card-title grey-text text-darken-4" > '.ucfirst($service->s_name).'<i class="material-icons right">close</i > </span >

<a class="btn disabled">'.$service->s_demography.'</a>
<a class="btn disabled">K'.$service->s_price.'</a><br>
<span class="grey-text">'.$service->s_info.'</span>
<div class="input-field">
<input type="text" name="quantity" class="form-control quantity" id="'.$service->s_id.'" / > <br >
<a name="add_cart" class="btn grey white-text add_cart" data-productname="'.$service->s_name.'" data-price="'.$service->s_price.'" data-productid="'.$service->s_id.'" > Add</a >
</div>
</div >
</div >
 
 ';
   }
   ?>
</section>
  <ul class="pagination center-align">
    <?php echo $this->pagination->create_links(); ?>
</ul>
</div>


<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/jquery/jqueryc.js"></script>

<script type="text/javascript">
$(document).ready(function(){

 $('#publish').click(function(){
  
 });

 $('.add_cart').click(function(){
  var product_id = $(this).data("productid");
  var product_name = $(this).data("productname");
  var product_price = $(this).data("price");
  var quantity = $('#' + product_id).val();
  if(quantity != '' && quantity > 0)
  {
   $.ajax({
    url:"<?php echo base_url(); ?>/cartadd",
    method:"POST",
    data:{s_id:product_id, s_name:product_name, s_price:product_price, quantity:quantity},
    success:function(data)
    {
     alert("Product Added into Cart");
     $('#cart_details').html(data);
     $('#' + product_id).val('');
    }
   });
  }
  else
  {
   alert("Please Enter quantity");
  }
 });

 $('#cart_details').load("<?php echo base_url();?>/cartload");

 $(document).on('click', '.remove_inventory', function(){
  var row_id = $(this).attr("id");
  if(confirm("Are you sure you want to remove this?"))
  {
   $.ajax({
    url:"<?php echo base_url(); ?>cartitemdel",
    method:"POST",
    data:{row_id:row_id},
    success:function(data)
    {
     alert("Product removed from Cart");
     $('#cart_details').html(data);
    }
   });
  }
  else
  {
   return false;
  }
 });

 $(document).on('click', '#clear_cart', function(){
  if(confirm("Are you sure you want to clear cart?"))
  {
   $.ajax({
    url:"<?php echo base_url(); ?>/clear",
    success:function(data)
    {
     alert("Your cart has been clear...");
     $('#cart_details').html(data);
    }
   });
  }
  else
  {
   return false;
  }
 });

});
</script>
</body>
</html>
