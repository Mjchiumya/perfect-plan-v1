<main>
 <div class="slider z-depth-2">
    <ul class="slides">
        <li>
            <img src="http://lorempixel.com/580/250/nature/1">
            <div class="caption center-align">       
            <h1>PERFECT PLAN</h1>           
            <h5 class="flow-text">Online Event Planner For All Occassions</h5>        
               <div id='box'>                 
               <div class='btn z-depth-3 red'><a class="white-text" href="<?php echo base_url();?>eventprofile">Create Event</a></div>
               </div>             
           <div>
         <code><p class="center-align">for full access you are required to sign up</p></code>
          </div>
        </div>    
    </li>      
      <li>
        <img src="http://lorempixel.com/580/250/nature/2"> <!-- random image -->
        <div class="caption left-align">
          <h3>creating event made easy..!</h3>          
		  <div class="row bgr">
		    <div class="col s2 white z-depth-1">
			   <h2 class="center-align grey-text">1<br><span class="grey-text">SignUp</span></h2>
		      
			 </div>
		    <div class="col s2 white offset-m2">
			  <h2 class="center-align grey-text darken-text-1">2<br><span class="grey-text">Login</span></h2>
			</div>
			 <div class="col s2 white offset-m2 z-depth-3">
			   <h2 class="center-align grey-text">3<br><span class="grey-text">Create</span></h2>
			 </div>
		  </div>
        </div>
      </li>
      <li>
        <img src="http://lorempixel.com/580/250/nature/3"> <!-- random image -->
        <div class="caption right-align">
          <h3>perfect plan for a perfect budget </h3>
          <h5 class="light grey-text text-lighten-3">plan event according to your bugdet.</h5>
        </div>
      </li>
      <li>
        <img src="http://lorempixel.com/580/250/nature/4"> <!-- random image -->
        <div class="caption center-align">
          <h3>Event planning made easy!</h3>
          <h3 class="light grey-text text-lighten-3">weddings >> birthday parties >> bridal showers .</h3>
        </div>
      </li>
    </ul>
  </div>  
</main>


<div class="wrapp z-depth-2 white">
 <div class="container">
  <div class="row">
    <div class="col s12 m3">
      <h3 class="left-align white-text licth">Category</h3>
      <ul>
        <?php foreach($categories as $cat){
        echo '<li><a class="grey-text lict" href="#">'.$cat->s_category.'</a></li>';
         }?>
      </ul>
    </div>

    <div class="col s12 m3">
      <h3 class="left-align white-text licth">Location</h3>
      <ul>
        <?php foreach($locations as $loc){
        echo '<li><a class="grey-text lict"href="#">'.$loc->s_demography.'</a></li>';
         }?>
      </ul>
    </div>
	 <div class="col s12 m3">
      <h3 class="left-align white-text licth">Products</h3>
      <ul>
        <?php foreach($products as $product){
        echo '<li><a class="grey-text lict" href="'.base_url().'service/'.$product->s_id.'">'.$product->s_name.'</a></li>';
         }?>
      </ul>
    </div>

    <div class="col s12 m3">
      <h3 class="left-align white-text licth">Business</h3>
       <ul>
        <?php foreach($business as $bus){
        echo '<li><a class="grey-text lict" href="'.base_url().'business/'.$bus->b_id.'">'
        .$bus->b_name.'</a></li>';
         }?>
      </ul>
    </div>

  </div> 
</div>
</div>

<div class="section"></div>
<div class="section"></div>
<div class="flex-container white">
<div class="row">
 <?php foreach($services as $service){
 echo '
   <div class="col s6 m3 flip-box">
    <div class="flip-box-inner">
    <div class="flip-box-front">
      <img class="responsive-img" width="100%" src="img/services/'.$service->image.'">
    </div>
    <div class="flip-box-back">
      <h3>'.$service->s_name.'</h3>
	  <div class="section hide-on-small-only"></div>
	  <a href="'.base_url().'service/'.$service->s_id.'" class="btnc success">view</a>
    </div>
  </div>
</div>';
 }?>
 <!-- end card-tree -->
   </div> 
  <br>
</div>

<div class="section"></div>
<div class="section"></div>
 <section class="prim nunu">  
  <div class="row">              
    <h2 class="center-align white-text text-darken-1">Register your Business</h2> 
     <div class="section hide-on-small-only"></div>     
	    <div class="container">
		<h3 class="white-text text-darken-1 center-align">Take your business to millions of customers and let them discover your magic today</h3>
		</div>		    
       <center>
         <a href="<?php echo base_url('coming');?>" class="center-align waves-effect waves-light btn-large btn red">Register here</a>
       </center>
  </div>
 </section>
<div class="section"></div>
<div class="row eview">
  <div class="column grey">
    <div class="pard">
      <p><i class="fa fa-user"></i></p>
      <h3>11+</h3>
      <p>Private Events</p>
    </div>
  </div>

  <div class="column grey">
    <div class="pard">
      <p><i class="fa fa-check"></i></p>
      <h3>55+</h3>
      <p>Public Events</p>
    </div>
  </div>

  <div class="column grey">
    <div class="pard">
      <p><i class="fa fa-smile-o"></i></p>
      <h3>100+</h3>
      <p>Users</p>
    </div>
  </div>

  <div class="column grey">
    <div class="pard">
      <p><i class="fa fa-coffee"></i></p>
      <h3>100+</h3>
      <p>Businessess</p>
    </div>
  </div>
</div>
