<div class="container white">  
  <?php echo validation_errors(); ?>
<?php echo form_open("formevtedit");?>
<div class="section"></div>
<div class="section"></div>

<h3 class="prim align-center>">EVENT EDITOR</h3>
 
<div class="row">
 <div class="input-field col s6">         
  <input  class="validate" name="organiser" type="text" />
  <label class="active" for="organiser">Organiser</label>
 </div>
 <div class="input-field col s6">
  <input class="validate" type="text" name="name"/ >
  <label for="name">Event name</label>
  </div>
</div>

<div class="row">
  <div class="input-field col s6">
  <input type="date" name="evtdate" >
</div>
    <div class="input-field col s6">
      <input class="validate" name="evtlocation" type="text"/ >
       <label for="evtlocation">Event Location</label>      
  </div>
 </div>
  
<div class="row">
 <div class="input-field col s6">
  <input name="evttime" type="time"> 
</div>
<div class="input-field col s6">
  <input name="evttype" type="text"/>
  <label for="name">Event Type</label>    
 </div>
</div>

<div class="row">
 <div class="input-field col s6">
  <input name="evtcharge" type="text"/ >
    <label for="evtcharge">Event Charge</label>  
  </div>
</div>  
<div class="input-field col s12">
  <textarea name="evtmsg" class="materialize-textarea"></textarea>
  <label for="evtcharge">Information</label>  
</div>


<center>
  <input type="submit" name="submit" class="btn grey">
  <a href="<?php echo base_url('user');?>" class="red btn"><i class="material-icons right">cancel</i> cancel</a> 
</center>
    
</form>  
</div>
<div class="section"></div>
