
  <div class="section"></div>
  <div class="section"></div>
  <div class="section"></div>
  <div class="section"></div>
  <div class="section"></div>
  
<center>     
<div class="container">
 <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;">

<?php echo form_open("login_user");?>           
<h5 class="prim">Account Login</h5>
<?php
      $success_msg = $this->session->flashdata('success_msg');   
      $error_msg = $this->session->flashdata('error_msg');          
         if($error_msg){
           ?>
          <div class="red-text">
           <?php echo $error_msg; ?>
              </div>
                <?php
                  }?>
         <?php if($success_msg){
           ?>
          <div class="alert">
           <?php echo $success_msg; ?>
              </div>
                <?php
                  }?> 

      <br>
            <div class='row'>
              <div class='input-field col s12'>
                <input class='validate'type='email' name='email' id='email' required />
                <label class="active" for='email'>Enter your email</label>
              </div>
            </div>

            <div class='row'>
              <div class='input-field col s12'>
                <input class='validate' type='password' name='password' id='password' required />
                <label class="active" for='password'>Enter your password</label>
              </div>
              <label style='float: right;'><br>
			<a class='pink-text' href='#!'><b>Forgot Password?</b></a>
							</label>
            </div>

            <br />
            <center>
              <div class='row'>
                <button type='submit' name='btn_login' class='col s12 btn btn-large waves-effect red'>Login</button>
                <center><b>Not registered ?</b> <br></b><a href="<?php echo base_url('register'); ?>" class="grey-text">Register here</a></center>
              </div>
            </center>
          </form>
        </div>
      </div>      
    </center>

    <div class="section"></div>
    <div class="section"></div>
 

     <!-- Compiled and minified JavaScript -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/jquery/jqueryc.js"></script>  
  </body>
</html>
