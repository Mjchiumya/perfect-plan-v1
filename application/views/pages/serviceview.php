<div class="base">
  
  <div class="container">
  <div class="col s12 m4 l5">
     <?php foreach($services as $serv):?>
      <div class="card-image">
        <div class="carousel carousel-slider center" data-indicators="true">
          <div class="carousel-fixed-item center">      
             <a class="btn white black-text darken-text-2">swipe</a>
          </div>

            <div class="carousel-item red white-text" href="#one!">      
              <img src="<?php echo base_url().'img/services/'. $serv['img1']?>" class="img-responsive">
            </div>

           <div class="carousel-item amber white-text" href="#two!">
            <img src="<?php echo base_url().'img/services/'. $serv['img2']?>" class="img-responsive">
           </div>

          <div class="carousel-item green white-text" href="#three!">
           <img src="<?php echo base_url().'img/services/'. $serv['img3']?>">
         </div>   
      </div>

    </div>
   </div>
</div>

 <div class="white col s12 m8 l7"> 

 <div class="container business bgr white">
  
   <?php 
   echo  '
   <h3><span class="card-title grey-text text-darken-1">'.$serv['s_name'].'<span class="card-title grey-text text-darken-1 right">K'.$serv['s_price'].'</span></span></h3>
    <a href="#" class="btn disabled"><i class="material-icons left">category</i>'.$serv['s_category'].'</a>
	<a href="'.base_url().'business/'.$serv['b_id'].'" class="grey-text btn grey lighten-2"><i class="material-icons left">person</i>owner</a>
	
	 <div class="hide-on-small-only section"></div>
     <p class="left-align grey-text text-darken-1">'.$serv['s_info'].'.</p>'
	 ;
	 endforeach;?>

   </div>  
		 
	<div class="section"></div>
		
     </div>
  </div>
  </div>
  
