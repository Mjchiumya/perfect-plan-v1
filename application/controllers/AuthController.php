<?php

/**
 * authController short summary.
 *
 * authController description.
 *
 * @version 1.0
 * @author acer
 */
class AuthController extends CI_Controller{
    //constructor
    public function __construct(){
		parent::__construct();
        $this->load->model('AuthModel');
        $this->load->library(array('cart','session','form_validation'));
        $this->load->helper(array('form','url'));
    }

    //create user account
    public function register_user(){
        $this->form_validation->set_rules('name','name','required|trim|max_length[20]');
        $this->form_validation->set_rules('email','email','required|trim|valid_email');
        $this->form_validation->set_rules('password','password','required|trim|max_length[30]|min_length[6]');
        $this->form_validation->set_rules('passcomfirm','passcomfirm','required|trim|matches[password]');
        $this->form_validation->set_rules('dob','dob','required|date');
        $this->form_validation->set_rules('mobile','mobile','required|numeric');

        if($this->form_validation->run()==true){
           $user=array(
                      'u_name'=>$this->input->post('name'),
                       'u_email'=>$this->input->post('email'),
                        'u_password'=>md5($this->input->post('password')),
                         'u_dob'=>$this->input->post('dob'),
                          'u_mobile'=>$this->input->post('mobile')
                           );

           $email_check=$this->AuthModel->email_check($user['u_email']);

        if($email_check != FALSE){
            $this->AuthModel->register_user($user);
           $this->session->set_flashdata('success_msg', 'Registered successfully..now login.');
           redirect('login');
            }else{
                $this->session->set_flashdata('error_msg', 'email already in use...');
                redirect('register');
            }
            //if form fails
        }else{
            $this->session->set_flashdata('error_msg', 'Error occured while registering...');
            redirect('register');
        }
    }

    //login user account
    public function login_user(){
        $user_login = array(
                         'user_email'=>$this->input->post('email'),
                         'user_password'=>md5($this->input->post('password'))
                          );
        $this->form_validation->set_rules('email','email','required');
        $this->form_validation->set_rules('email','email','required');
        $data=$this->AuthModel->login_user($user_login['user_email'],$user_login['user_password']);
        if($data){
            $this->session->set_userdata('user_id',$data['u_id']);
            $this->session->set_userdata('user_email',$data['u_email']);
            $this->session->set_userdata('user_name',$data['u_name']);
            redirect('user');
        }
        else{
            $this->session->set_flashdata('error_msg', 'incorrect details,Try again..');
            $this->load->view("temps/header");
            $this->load->view("pages/login1.php");
        }
    }

    //logout user account
    public function user_logout(){
        $this->session->sess_destroy();
        redirect('login', 'refresh');
    }

    //deactivate account
    public function user_deactivate($uid){
        if($this->AuthModel->account_deactivate($uid) == true){
            $this->session->sess_destroy();
            redirect('home');
        }else{
            redirect('user');
        }
    }
}
