<?php

/**
 * EventController short summary.
 *
 * EventController description.
 *
 * @version 1.0
 * @author acer
 */
class EventController extends CI_Controller{

    //constructor
    public function __construct(){
		parent::__construct();
        $this->load->model(array('EventModel','Mymodel'));
        $this->load->library(array('cart','session','form_validation'));
        $this->load->helper(array('form','url'));
    }

    //show create event form
    public function event_input(){
        if($this->session->user_id != null){
            $this->load->view("temps/header");
            $this->load->view("pages/event");
            $this->load->view("temps/footer");
        }else{
            redirect('login');
        }
    }

    //event view
    public function eventview($eid){
        $uid = intval($this->session->user_id);
        $data['event'] = $this->EventModel->read_event_with_id($uid,$eid);
        $data['products'] =$this->Mymodel->load_db_cart($eid);
        $data['eid'] = array('eid'=>$eid);
        $this->load->view('temps/header');
        $this->load->view('pages/eventview',$data);
    }

    //event editor form
    public function event_editor($eid){
        $uid = intval($this->session->user_id);
        $data['event'] = $this->EventModel->read_event_with_id($uid,$eid);
        $data['eid'] = array('eid'=>$eid);
        $this->load->view("temps/header");
        $this->load->view("pages/eventeditor",$data);
        $this->load->view("temps/footer");
    }

    //live event view
    public function liveEventView($eid){
        $uid = intval($this->session->user_id);
        $data['event'] = $this->EventModel->read_event_with_id($uid,$eid);
        $data['products'] =$this->Mymodel->load_db_cart($eid);
        $data['eid'] = array('eid'=>$eid);
        $this->load->view('temps/header');
        $this->load->view('pages/liveeventview',$data);
    }

    //delete event
    public function del_event($eid){
        $uid = intval($this->session->user_id);
        if ($this->EventModel->del_event_mode($eid,$uid)) {
            redirect('user','refresh');
        }else{

        }
    }

}
