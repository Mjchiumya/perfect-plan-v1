<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * pagesController short summary.
 *
 * pagesController description.
 *
 * @version 1.0
 * @author acer
 */
class PagesController extends CI_Controller{

    //constructor
    public function __construct(){
		parent::__construct();
        $this->load->model('Mymodel');
        $this->load->library(array('cart','session','form_validation'));
        $this->load->helper(array('form','url'));
    }

    //index page
    public function index(){
        $data['products']=$this->Mymodel->get_service();
        $data['services'] =$this->Mymodel->get_three();
        $data['events']=$this->Mymodel->all_events();
        $data['categories']=$this->Mymodel->get_categories();
        $data['locations']=$this->Mymodel->get_demography();
        $data['business']=$this->Mymodel->get_business();
        $this->load->view('temps/header');
        $this->load->view('pages/home',$data);
        $this->load->view('temps/footer');
    }
    //login form view
    public function login_view(){
        if($this->session->user_id != null) {
            redirect('user','refresh');
        }else{
            $this->load->view("temps/header");
            $this->load->view("pages/login1.php");
        }
    }
    //register view
    public function register_view(){
        if($this->session->user_id != null) {
            redirect('user','refresh');
        }else{
            $this->load->view("temps/header");
            $this->load->view("pages/register2.php");
        }
    }
    //user account
    public function user_profile(){
        if ($this->session->user_id != null) {
            $data['evt'] = $this->Mymodel->readEvent(intval($this->session->user_id));
            $data['drafts'] = $this->Mymodel->readDraftEvent(intval($this->session->user_id));
			$data['cancel'] = $this->Mymodel->readCancelledEvent(intval($this->session->user_id));
            $this->load->view("temps/header");
            $this->load->view('pages/userview.php',$data);
        }else{
            redirect('login','refresh');
        }
    }
    //unconstructed page/service
    public function comingsoon(){
        $this->load->view("temps/header");
        $this->load->view('pages/coming');
    }
}
