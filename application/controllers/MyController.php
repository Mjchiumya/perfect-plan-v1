<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MyController extends CI_Controller {

    //constructor
    public function __construct(){
		parent::__construct();
        $this->load->model('Mymodel');
        $this->load->library(array('cart','session','form_validation'));
        $this->load->helper(array('form','url'));
    }
 

//business view
public function businessView($bid){
  $data['business'] = $this->Mymodel->get_businessId($bid);
    $data['services'] = $this->Mymodel->get_business_services($bid);
  $this->load->view('temps/header');
   $this->load->view('pages/businessview',$data);
   $this->load->view('temps/footer');
}
//service view
public function serviceView($sid){
  $data['services'] = $this->Mymodel->get_serviceId($sid);
  $this->load->view('temps/header');
   $this->load->view('pages/serviceview',$data);
   $this->load->view('temps/footer');
}



//
public function event_form(){
  $this->form_validation->set_rules('organiser','Organiser', 'required');
    $this->form_validation->set_rules('name','Name', 'required');
     $this->form_validation->set_rules('evtdate','Evtdate', 'required');
      $this->form_validation->set_rules('evtlocation','Evtlocation', 'required');
       $this->form_validation->set_rules('evttime','Evttime', 'required');
        $this->form_validation->set_rules('evttype','Evttype', 'required');
        $this->form_validation->set_rules('evtcharge','Evtmsg', 'required');
        $this->form_validation->set_rules('evtmsg','Evtmsg', 'required');

        $organiser = $this->input->post('organiser');
        $name = $this->input->post('name');
        $evtdate = $this->input->post('evtdate');
        $evtlocation = $this->input->post('evtlocation');
        $evttime = $this->input->post('evttime');
        $evttype = $this->input->post('evttype');
        $evtmsg = $this->input->post('evtmsg');
        $evtcharge = $this->input->post('evtcharge');
        $uid = $this->session->user_id;
    if ($this->form_validation->run() == FALSE){
              echo $this->event_input();
                }else
                   {
                       $data  = array(
                         'e_name' => $name,
                          'e_organiser'=>$organiser,
                           'e_date'=>$evtdate,
                            'e_time'=>$evttime,
                              'e_location'=>$evtlocation,
                          'e_type'=>$evttype,
                          'e_notes'=>$evtmsg,
                          'e_charge'=>floatval($evtcharge),
                          'uid' =>$uid,
                          'e_status'=>'draft'
                            );

                       if ($data != null) {
                        $this->Mymodel->insert_toevt($data);
                       }

                     redirect('user', 'refresh');
                   }
}

public function evtedit_formval(){
  $this->form_validation->set_rules('organiser','Organiser', 'required');
    $this->form_validation->set_rules('name','Name', 'required');
     $this->form_validation->set_rules('evtdate','Evtdate', 'required');
      $this->form_validation->set_rules('evtlocation','Evtlocation','required');
       $this->form_validation->set_rules('evttime','Evttime', 'required');
        $this->form_validation->set_rules('evttype','Evttype', 'required');
        $this->form_validation->set_rules('evtcharge','Evtmsg', 'required');
        $this->form_validation->set_rules('evtmsg','Evtmsg', 'required');

        $organiser = $this->input->post('organiser');
        $name = $this->input->post('name');
        $evtdate = $this->input->post('evtdate');
        $evtlocation = $this->input->post('evtlocation');
        $evttime = $this->input->post('evttime');
        $evttype = $this->input->post('evttype');
        $evtmsg = $this->input->post('evtmsg');
        $evtcharge = $this->input->post('evtcharge');

    if ($this->form_validation->run() == FALSE){
              redirect('user','refresh');
                }else
                   {
                       $data  = array(
                         'e_name' => $name,
                          'e_organiser'=>$organiser,
                           'e_date'=>$evtdate,
                            'e_time'=>$evttime,
                              'e_location'=>$evtlocation,
                          'e_type'=>$evttype,
                          'e_notes'=>$evtmsg,
                          'e_charge'=>floatval($evtcharge)
                            );

                       if ($data != null) {
                        $this->Mymodel->update_toevt($data,$eid);
                       }

                     redirect('user', 'refresh');
                   }
}


 public function orderServices($eid){
     if($this->Mymodel->serviceOrder($eid)){
	       $this->session->set_flashdata('order_msg', 'order sent!');
	 	 redirect('user');
	 }else{
	       $this->session->set_flashdata('order_msg', 'empty order! please add services to your event.');
	 	 redirect('user');
	 }
 }
 public function cancelOrder($eid){
     if($this->Mymodel->orderCancel($eid)){
	       $this->session->set_flashdata('order_msg', 'order cancelled!');
	 	 redirect('user');
	 }else{
	 	 redirect('user');
	 }
 }




}







